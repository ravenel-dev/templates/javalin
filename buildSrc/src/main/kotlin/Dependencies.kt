object Versions {
  const val javalin = "2.8.0"
  const val junit = "5.4.2"
}

object Libs {
  const val javalin = "io.javalin:javalin:${Versions.javalin}"
  const val junit = "org.junit.jupiter:junit-jupiter:${Versions.junit}"
}
