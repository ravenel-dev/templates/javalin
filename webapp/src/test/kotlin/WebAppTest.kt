import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.net.HttpURLConnection
import java.net.URL

class WebAppTest {

  lateinit var webapp: WebApp

  @BeforeEach
  fun beforeEach() {
    webapp = WebApp()
  }

  @AfterEach
  fun afterEach() {
    webapp.stop()
  }

  @Test
  fun canStartWebappWithPort() {
    webapp.start(9000)
  }

  @Test
  fun canStopWebApp() {
    webapp.stop()
  }

  @Test
  fun webAppIsStarted() {
    webapp.start()
    val url = URL("http://127.0.0.1:8080")
    val connection = url.openConnection() as HttpURLConnection
    connection.requestMethod = "GET"
    val status = connection.responseCode
    assertEquals(HttpURLConnection.HTTP_NOT_FOUND, status)
  }
}
