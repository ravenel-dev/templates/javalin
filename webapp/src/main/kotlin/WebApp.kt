import io.javalin.Javalin

class WebApp {
  fun start(port: Int = 8080) {
    val app = Javalin.create().start(port)
  }
  fun stop() {}
}
